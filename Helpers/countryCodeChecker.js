import { countriesCode } from "./Constants";

export const typeChecker = code => countriesCode.indexOf(code) === -1 ? Messages.serverError : null;
