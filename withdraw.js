const db = require('./database');
const config = require('./config');
const { types, countriesCode } = require('./Helpers/Constants');
const onsiteGiftcardWithdraw = require('./Withdraw/onsiteGiftcardWithdraw');

let minEarnText = `You must earn at least ${config.withdraw.minEarnedToWithdraw} coins ($${(
  config.withdraw.minEarnedToWithdraw / 1000
).toFixed(
  2
)}) through the offer walls before withdrawing.<br>This is to prevent abuse of the site bonuses. Please contact staff with any questions.`;

module.exports = {
  onsiteGiftcardWithdraw: function (socket, socketuser) {
    return function (data) {
      const withdraw = new Withdraw(socket, socketuser).operate();
      return withdraw;
    };
  },
};
