module.exports = {
    insertPendingSiteGiftCardWithdraw: db.insertSiteGiftCardWithdraw(
        socketuser.gainid,
        coinAmount,
        type,
        countryCode,
        utils.getIsoString(),
        null,
        (err, result) => {
            if (errorValidator(err))
                feedback(Messages.serverError)

            socket.emit('withdrawal', {
                coins: coinAmount,
            });

            Notifications.storeNotification(
                socketuser.gainid,
                'Info',
                'withdrawal',
                `Your ${type} Gift Card withdrawal worth ${coinAmount} coins is pending.`
            );

            if (outOfStock)
                feedback(Messages.succcededButOutOfStock);
            else
                feedback(Messages.staffMemberNotified);

            emitBalance(socketuser.gainid);
        }
    )
}