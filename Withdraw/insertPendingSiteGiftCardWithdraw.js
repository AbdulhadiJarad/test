module.exports = {
    insertPendingSiteGiftCardWithdraw: db.insertPendingSiteGiftCardWithdraw(
        socketuser.gainid,
        coinAmount,
        type,
        countryCode,
        utils.getIsoString(),
        null,
        (err, result) => {
            if (errorValidator(err))
                feedback(Messages.serverError)

            socket.emit('withdrawalPending', {
                coins: coinAmount,
            });

            Notifications.storeNotification(
                socketuser.gainid,
                'Info',
                'pendingwithdrawal',
                `Your ${type} Gift Card withdrawal worth ${coinAmount} coins is pending.`
            );

            if (outOfStock) 
                feedback(Messages.succcededButOutOfStock);
             else 
                feedback(Messages.staffMemberNotified);

            emitBalance(socketuser.gainid);
        }
    )
}