const getAccountStanding = require('./getAccountStanding') 
module.exports = {
    onsiteGiftcardWithdraw: (socket, socketuser) => {
        return function (data) {
            let feedbackType = '';

            function feedback(feedback) {
                socket.emit('withdrawFeedback', feedback, feedbackType);
            }

            if (!data) return feedback(Messages.serverError);

            let type = data.type;
            let coinAmount = parseInt(data.coinAmount);
            let countryCode = data.countryCode || 'WW';

            feedbackType = type;
            const isNotValidType = typeChecker(type, types);
            const isNotValidCode = typeChecker(countryCode, countriesCode);
            const isAuthenticated = socketUserChecker(socketuser)
            if (isNotValidType || isNotValidCode || !type || !isAuthenticated) 
                return feedback(isValidType);

            if (isNaN(coinAmount) || !coinAmount) 
                return feedback(Messages.selectAmmount);
        };
    }
}