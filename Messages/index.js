const messages = {
    bannedContry: `You are currently banned from withdrawing, please contact staff if you believe this is a mistake.`,
    serverError: `An error occurred, please try again`,
    lackBalance: `You don't have enough balance!`,
    outOfStock: `This card is currently out of stock. Please choose another.`,
    emailNotVerrified: `You must verify your E-mail address before requesting a withdrawal!`,
    selectAmmount: `Please select an amount!`,
    succcededButOutOfStock: `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`,
    staffMemberNotified: `Success!<br>Since you are not verified, a staff member has been notified and will review your withdrawal shortly! Check your Profile page to view your redemption code after the withdrawal has been approved.<br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`,
    GiftCardUnavailableError : 'gift card is bot available'
}